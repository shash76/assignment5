#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

# List of dictionaries to store all of the book information
books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
    return jsonify(books)

@app.route('/')
@app.route('/book/')
def showBook():
    # Use showBook.html to render the template
    return render_template('showBook.html', books = books)
	
@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
    # Verify if it was a POST request
    if request.method == 'POST':
        # Get the name of the new book from the form
        new_name = request.form['name']
        # Create dictionary to store the new book
        new_book = {'title' : new_name, 'id' : '0'}
        id_list = []
        # Add all of the ID's from the books to a list
        for b in books:
            id_list.append(int(b['id']))
        # Sort the list to ease finding new ID
        id_list.sort()
        new_id = 1
        # Find new ID by looping to find smallest available ID value
        for num in id_list:
            if num <= new_id:
                new_id += 1
        new_book['id'] = str(new_id)
        # Add new book to the entire books collection
        books.append(new_book)
        return redirect(url_for('showBook'))
    # Use newBook.html to render the template for the GET request
    else:
        return render_template('newBook.html')

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
    # Verify if it was a POST request
    if request.method == 'POST':
        # Get the new name for the book from the form
        new_name = request.form['name']
        # Loop until correct book is found and replace name
        for b in books:
            if b["id"] == str(book_id):
                b["title"] = new_name
        return redirect(url_for('showBook'))
    # Use editBook.html to render the template for the GET request
    else:
        return render_template('editBook.html')
	
@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
    # Verify if it was a POST request
    if request.method == 'POST':
        # Loop through all books to find correct one to delete
        for b in books:
            if b['id'] == str(book_id):
                books.remove(b)
        return redirect(url_for('showBook'))
    else:
        # Loop through all books to find title of book requesting deletion
        for b in books:
            if b['id'] == str(book_id):
                title = b['title']
        # Use deleteBook.html to render the template for the GET request
        return render_template('deleteBook.html', title = title)

# Main
if __name__ == '__main__':
    app.debug = True
    app.run(host = '0.0.0.0', port = 5000)
